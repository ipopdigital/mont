
$(document).ready(function(){

    var scrollPostion = window.scrollY;
    var container = document.querySelector("#js-learn-achieve");
    if (!container) return;
    window.addEventListener("scroll", function() {
        scrollPostion = this.scrollY;
        container.style.setProperty('--x', (-scrollPostion * 0.2) + "px" );
    })
});


jQuery(document).ready(function(){
    // function dpResize(){
    //     var maxHeight = 0;
    //     $('.dp-paragraph').each(function(){  
    //         $('.dp-paragraph',this).height('auto');
    //         // Select and loop the elements you want to equalise
    //             // If this box is higher than the cached highest then store it
    //         if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
    //     });
    //     // Set the height of all those children to whichever was highest 
    //     $('.dp-paragraph',this).height(maxHeight);
    //     console.log(maxHeight);    
    // }
    // if ($('.dp-paragraph')){
    //     // Cache the highest
    //     dpResize();
    
    //     $(window).resize(function(){
    //         dpResize();
    //     });
        
    // }

    function cardsSwiperInit(swiperSelected){
        if ($('#dp-next')){
            $("#dp-next").click(function(){
                var total = $("."+swiperSelected).length;
                $("#dp-slider ."+swiperSelected+":first-child").hide().appendTo("#dp-slider").fadeIn();
                $.each($('.'+swiperSelected), function (index, swiperSelected) {
                    $(swiperSelected).attr('data-position', index + 1);
                });
            });
        }
        if ($('#dp-prev')){
            $("#dp-prev").click(function(){
                var total = $("."+swiperSelected).length;
                $("#dp-slider ."+swiperSelected+":last-child").hide().prependTo("#dp-slider").fadeIn();
                $.each($('.dp_itemall'), function (index, swiperSelected) {
                    $(swiperSelected).attr('data-position', index + 1);
                });
            });
        }

        if ($('#dp-nextksone')){
            $("#dp-nextksone").click(function(){
                var total = $(".dp_itemksone").length;
                $("#dp-sliderksone .dp_itemksone:first-child").hide().appendTo("#dp-sliderksone").fadeIn();
                $.each($('.dp_itemksone'), function (index, dp_itemksone) {
                    $('dp_itemksone').attr('data-position', index + 1);
                });
            });
        }
        if ($('#dp-prevksone')){
            $("#dp-prevksone").click(function(){
                var total = $(".dp_itemksone").length;
                $("#dp-sliderksone .dp_itemksone:last-child").hide().prependTo("#dp-sliderksone").fadeIn();
                $.each($('.dp_itemksone'), function (index, swiperSelected) {
                    $(swiperSelected).attr('data-position', index + 1);
                });
            });
        }

        if ($('#dp-nextkstwo')){
            $("#dp-nextkstwo").click(function(){
                
                var total = $(".dp_itemkstwo").length;
         
                $("#dp-sliderkstwo .dp_itemkstwo:first-child").hide().appendTo("#dp-sliderkstwo").fadeIn();
                $.each($('.dp_itemkstwo'), function (index, dp_itemksone) {
                    $('dp_itemkstwo').attr('data-position', index + 1);
                });
            });
        }
        if ($('#dp-prevkstwo')){
            $("#dp-prevkstwo").click(function(){
                var total = $(".dp_itemkstwo").length;
                $("#dp-sliderkstwo .dp_itemkstwo:last-child").hide().prependTo("#dp-sliderkstwo").fadeIn();
                $.each($('.dp_itemkstwo'), function (index, swiperSelected) {
                    $(swiperSelected).attr('data-position', index + 1);
                });
            });
        }
    }

    cardsSwiperInit('dp_itemall');

    $('#ksone').click(function(){
        $('#dp-prev, #dp-next,#slider').hide();
        $('#sliderkstwo, #dp-nextkstwo, #dp-prevkstwo').hide();
        $('#sliderksone, #dp-nextksone, #dp-prevksone').show();
    });
    $('#kstwo').click(function(){
       
        $('#dp-prev, #dp-next,#slider').hide();
        $('#sliderksone, #dp-nextksone, #dp-prevksone').hide();
        $('#sliderkstwo, #dp-nextkstwo, #dp-prevkstwo').show();
    });
});

// Mobile Menu
if ($('#js-mobile-menu-btn')){
    $('#js-mobile-menu-btn').click(function(e){
        e.stopPropagation();
        console.log('open');
        var effect = $(this).attr('data-effect');
        openMenu(effect);
    });
}


function openMenu(effect) {
    var menu = $('#js-menu-container');
    menu.toggleClass(effect);
    $('.js-menu-close').toggleClass('js-menu-close-pusher');
    $('.js-menu-close-pusher').click(function(){
        closeMenu();
        console.log('close');
    });
    menu.toggleClass('st-menu-open');
}

function closeMenu(el) {
    var menu = $('#js-menu-container');
    // if the click target has this class then we close the menu by removing all the classes
    var effect = $('#js-mobile-menu-btn').attr('data-effect');
    menu.removeClass(effect);
    menu.removeClass('st-menu-open');
    $('.js-menu-close').removeClass('js-menu-close-pusher');
    // console.log(el.target);
  
}


// Calendar


//calendar - calling the file events.xml
//retrieve the parent slug from the jade file, select the appropriate events xml file
//which will determine the appropriate calendar
$(document).ready(function(){
    $(window).on('load', function() {
        // var handleCalendar = $('#mycalendar').attr('handle');
    
        // if (handleCalendar == 'combe-down') {   
        if ($('#defaultCalendar')[0]) {
            
            var cal = $('#defaultCalendar').monthly({
                mode: 'event',
                dataType: 'xml',
                xmlUrl: window.siteUrl+'events-calendar-'+calendarEvents+'.xml'
            });
        }
        
      
       
      
    });    
});
if( window.location.href.indexOf('arrangesuccess') !== -1 ){
    UIkit.notification({
        message: '<span uk-icon="icon: check"></span> Thank you for your message a member of staff will get back to you shortly',
        timeout: 10000,
       // status:'primary'
    });
}