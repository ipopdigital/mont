// **** Section intent is to animate the text line by line even when broken by wrap instead of linebreak*** //


// 1: Make sure the text you're passing to this function does NOT have any HTML attributes within (<h1> etc).

// 1b: example: .textSingleLineFadeIn(data-animation="fadeInKeyFrame") 

// 2: The resulting lines will be output within <span> tags within the element.

// 3: To add any alternative animations, in selector.push below within the window.scroll, add the class (comma separated)

// Homepage/first animation on page - fadein (triggers before scroll).

setTimeout(function(){ 
    var lines = isTextWrapped($('.textSingleLineFadeInFirst'));
    enterByLine($('.textSingleLineFadeInFirst'), lines, 1000, $('.textSingleLineFadeInFirst').attr('data-animation'));
}, 2000);            

$(window).scroll(function(){
    let waitingOnAnimRequest = false
    if (!waitingOnAnimRequest) {
        //One way to resolve this is using a requestAnimationFrame to decide when our callback gets fired. 
        //This is another window-level function where you may queue up callbacks for the browser to call.
        //When it feels it is ready to execute those functions without un-buttery-smoothing your scrolling experience, it will fire them off. 
        window.requestAnimationFrame(() => {
            var selector = [];
            selector.push('.textSingleLineFadeIn');
            //On scroll reach (selector array, offset, delay between each line, animation (keyframe) to be added)
            
            onScrollReachTextWrap(selector, 600, 500);
            waitingOnAnimRequest = false
        });
        waitingOnAnimRequest = true
    }
});


// Get wrapped text broken into an array by line...
function isTextWrapped(div) {
    var wraps = [];
    var words = div.text().split(" ");
    div.text(words[0]);
    var height = div.height();

    //For each word, split it by spaces
    for (var i = 1; i < words.length; i++) {
      div.text(div.text() + " " + words[i]);
      //If div height changes when you add the word back in, it has wrapped
      if (div.height() > height) {
        height = div.height();

        //Get the length of the text already included within wraps...
        var currentLength = 0;
        if (wraps.length != 0){
            wraps.forEach(function(currentWrap){
                currentLength = currentLength + currentWrap.length
            }); 
        }
        //Add to the array the substring from the point just added to the array until the end of the length of the words to be added
        wraps.push(div.text().substring(currentLength, div.text().length - words[i].length));
      }
    }
    //Add in the final line
    var pinultimateLength = 0;
    if (wraps.length != 0){   
        wraps.forEach(function(currentWrap){
            pinultimateLength = pinultimateLength + currentWrap.length
           
        });
        
    }
    //Get the remainder of the sentence (part on the very last line as not included in the breaks)
    wraps.push(div.text().substring(pinultimateLength, div.text().length));
    //Hide original text content of this element
    div.text('');
    $(div).css('opacity','1');
    $(div).css('min-height', height);
    return wraps;
}


//Function to make the actual CSS text entrance
function enterByLine (targetClass, lines, timeOut, cssAnimationClass){
   
    var nextLine = lines.shift();
    if(nextLine){
        var newLine = $('<span class="'+cssAnimationClass+'">' + nextLine + '</span>');
        $(targetClass).append(newLine);
        setTimeout(function(){ 
            enterByLine(targetClass, lines, timeOut, cssAnimationClass);   
        }, timeOut);
    }
}


// Adding/completing the designated effect for the wrapped text entrance when reached on scroll.
function onScrollReachTextWrap(selector, offset, timeOut) {

    var this_top;
    var height;
    var top;
    //If no offset, set one as 0 so that its initialised
    if(!offset) { var offset = 0; } 
    //Get scroll height
    //console.log(this);
    top = $(window).scrollTop();
    //For each of the selector element (class you're looking for)
    selector.forEach(function(item){
        $(item).each(function(currentItem){
           
            //Set position of where on page you want it to trigger the event
            this_top = $(this).offset().top - offset;
            height   = $(this).height();
         
            // Scrolled within current section & doesn't already have the class
            if (top >= this_top && !$(this).hasClass('animationComplete')) {
            
                //Targeted class, lines is the return of isTextWrapped, the number is the length of time between each
                //transitioned line entering, and the class added is the one with the CSS keyframe animation
              

                var lines = isTextWrapped($(this));
               
                enterByLine($(this), lines, timeOut, $(this).attr('data-animation'));
                $(this).addClass('animationComplete');
            }
        });
    });
}

// **** Section 2 -  *** //

// CSS Animation driven swiper block

//1: Example use in html:

// .swipe-in-block-left
//     .swipe(data-delay='{{loop.index / 2}}')
//     .swipe2(data-delay="{{loop.index / 2}}")
//     .inner



$('.swipe-in-block-left').each(function(key){
    //creating a delay for the animation
    //key starts at 0. The second iteration of the loop will be 1*0.2
    //The % sign divides it down so that key is either a 1 or a 0 (integer value).
    //Doing key %2 means only the even divs that have this on will be delayed. 
    //Purpose of this: if you have a left & right div all the way down the page, the left one will go then the right.
    
    
   

    //this instance of swipe in block left
    var wrap = this;
    var offset = 0;

    // first Swiper
    if ($('.swipe')){
     
        console.log('swipe in block left detected');
        var swipe = $(wrap).find('.swipe');
       
        var delayTime = $(swipe).attr('data-delay');
        if (delayTime != 0) {
            swipe.css('animation-delay', delayTime+'s');
        }
        swipe.addClass('transformScale');
    }
   
    //Second Swiper
    if ($('.swipe2')){
 
        console.log('swipe in block left detected');
        var swipe2 = $(wrap).find('.swipe2');
       
    
        var delayTime2 = $(swipe2).attr('data-delay');
        
        var delayTime2 = parseFloat(delayTime2) + 1.25;
        if (delayTime2 != 0) {
            swipe2.css('animation-delay', delayTime2+'s');
        }
        swipe2.addClass('transforReverse');
    }
   
});
 

// Anchor Links
$(".js-anchor-link").click(function(e) {
    e.preventDefault();
    var anchorID = $(this).attr('href');
    $('html, body').animate({
    //scroll to the top of anchiorid. Get it into position. Can add - navheight to the top
        scrollTop: $(anchorID).offset().top - 110
    }, 1000);
});